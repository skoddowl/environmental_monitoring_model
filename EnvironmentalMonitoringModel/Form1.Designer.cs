﻿namespace EnvironmentalMonitoringModel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.addEnterpriseButton = new System.Windows.Forms.Button();
            this.cityNameLabel = new System.Windows.Forms.Label();
            this.elapsedDaysLabel = new System.Windows.Forms.Label();
            this.currentEmissionsLabel = new System.Windows.Forms.Label();
            this.permissibleEmissionsLabel = new System.Windows.Forms.Label();
            this.nextDayButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.penaltyDaysCarsLabel = new System.Windows.Forms.Label();
            this.numberOfCarsLabel = new System.Windows.Forms.Label();
            this.numberOfEnterprisesLabel = new System.Windows.Forms.Label();
            this.budgetLabel = new System.Windows.Forms.Label();
            this.limitCarsButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.normalWeatherRadioButton = new System.Windows.Forms.RadioButton();
            this.rainyWeatherRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // gmap
            // 
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = false;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(12, 12);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 13;
            this.gmap.MinZoom = 9;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(632, 349);
            this.gmap.TabIndex = 0;
            this.gmap.Zoom = 12D;
            this.gmap.OnMarkerClick += new GMap.NET.WindowsForms.MarkerClick(this.gmap_OnMarkerClick);
            // 
            // addEnterpriseButton
            // 
            this.addEnterpriseButton.Location = new System.Drawing.Point(668, 303);
            this.addEnterpriseButton.Name = "addEnterpriseButton";
            this.addEnterpriseButton.Size = new System.Drawing.Size(137, 23);
            this.addEnterpriseButton.TabIndex = 1;
            this.addEnterpriseButton.Text = "Додати підприємство";
            this.addEnterpriseButton.UseVisualStyleBackColor = true;
            this.addEnterpriseButton.Click += new System.EventHandler(this.addEnterpriseButton_Click);
            // 
            // cityNameLabel
            // 
            this.cityNameLabel.AutoSize = true;
            this.cityNameLabel.Location = new System.Drawing.Point(6, 25);
            this.cityNameLabel.Name = "cityNameLabel";
            this.cityNameLabel.Size = new System.Drawing.Size(82, 13);
            this.cityNameLabel.TabIndex = 2;
            this.cityNameLabel.Text = "Місто Черкаси";
            // 
            // elapsedDaysLabel
            // 
            this.elapsedDaysLabel.AutoSize = true;
            this.elapsedDaysLabel.Location = new System.Drawing.Point(6, 47);
            this.elapsedDaysLabel.Name = "elapsedDaysLabel";
            this.elapsedDaysLabel.Size = new System.Drawing.Size(88, 13);
            this.elapsedDaysLabel.TabIndex = 2;
            this.elapsedDaysLabel.Text = "Пройшло днів: 0";
            // 
            // currentEmissionsLabel
            // 
            this.currentEmissionsLabel.AutoSize = true;
            this.currentEmissionsLabel.Location = new System.Drawing.Point(6, 91);
            this.currentEmissionsLabel.Name = "currentEmissionsLabel";
            this.currentEmissionsLabel.Size = new System.Drawing.Size(190, 13);
            this.currentEmissionsLabel.TabIndex = 2;
            this.currentEmissionsLabel.Text = "Кількість викидів на данний момент";
            // 
            // permissibleEmissionsLabel
            // 
            this.permissibleEmissionsLabel.AutoSize = true;
            this.permissibleEmissionsLabel.Location = new System.Drawing.Point(6, 69);
            this.permissibleEmissionsLabel.Name = "permissibleEmissionsLabel";
            this.permissibleEmissionsLabel.Size = new System.Drawing.Size(156, 13);
            this.permissibleEmissionsLabel.TabIndex = 2;
            this.permissibleEmissionsLabel.Text = "Кількість допустимих викидів";
            // 
            // nextDayButton
            // 
            this.nextDayButton.Location = new System.Drawing.Point(823, 303);
            this.nextDayButton.Name = "nextDayButton";
            this.nextDayButton.Size = new System.Drawing.Size(137, 23);
            this.nextDayButton.TabIndex = 3;
            this.nextDayButton.Text = "Наступний день";
            this.nextDayButton.UseVisualStyleBackColor = true;
            this.nextDayButton.Click += new System.EventHandler(this.nextDayButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.penaltyDaysCarsLabel);
            this.groupBox1.Controls.Add(this.numberOfCarsLabel);
            this.groupBox1.Controls.Add(this.numberOfEnterprisesLabel);
            this.groupBox1.Controls.Add(this.budgetLabel);
            this.groupBox1.Controls.Add(this.cityNameLabel);
            this.groupBox1.Controls.Add(this.elapsedDaysLabel);
            this.groupBox1.Controls.Add(this.permissibleEmissionsLabel);
            this.groupBox1.Controls.Add(this.currentEmissionsLabel);
            this.groupBox1.Location = new System.Drawing.Point(668, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(291, 212);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Інформація";
            // 
            // penaltyDaysCarsLabel
            // 
            this.penaltyDaysCarsLabel.AutoSize = true;
            this.penaltyDaysCarsLabel.Location = new System.Drawing.Point(6, 181);
            this.penaltyDaysCarsLabel.Name = "penaltyDaysCarsLabel";
            this.penaltyDaysCarsLabel.Size = new System.Drawing.Size(188, 13);
            this.penaltyDaysCarsLabel.TabIndex = 3;
            this.penaltyDaysCarsLabel.Text = "Обмеження руху автомобілів: 0 днів";
            // 
            // numberOfCarsLabel
            // 
            this.numberOfCarsLabel.AutoSize = true;
            this.numberOfCarsLabel.Location = new System.Drawing.Point(6, 159);
            this.numberOfCarsLabel.Name = "numberOfCarsLabel";
            this.numberOfCarsLabel.Size = new System.Drawing.Size(115, 13);
            this.numberOfCarsLabel.TabIndex = 3;
            this.numberOfCarsLabel.Text = "Кількість автомобілів";
            // 
            // numberOfEnterprisesLabel
            // 
            this.numberOfEnterprisesLabel.AutoSize = true;
            this.numberOfEnterprisesLabel.Location = new System.Drawing.Point(6, 136);
            this.numberOfEnterprisesLabel.Name = "numberOfEnterprisesLabel";
            this.numberOfEnterprisesLabel.Size = new System.Drawing.Size(131, 13);
            this.numberOfEnterprisesLabel.TabIndex = 3;
            this.numberOfEnterprisesLabel.Text = "Кількість підприємств: 0";
            // 
            // budgetLabel
            // 
            this.budgetLabel.AutoSize = true;
            this.budgetLabel.Location = new System.Drawing.Point(6, 113);
            this.budgetLabel.Name = "budgetLabel";
            this.budgetLabel.Size = new System.Drawing.Size(77, 13);
            this.budgetLabel.TabIndex = 3;
            this.budgetLabel.Text = "Бюджет міста";
            // 
            // limitCarsButton
            // 
            this.limitCarsButton.Location = new System.Drawing.Point(887, 268);
            this.limitCarsButton.Name = "limitCarsButton";
            this.limitCarsButton.Size = new System.Drawing.Size(72, 23);
            this.limitCarsButton.TabIndex = 5;
            this.limitCarsButton.Text = "OK";
            this.limitCarsButton.UseVisualStyleBackColor = true;
            this.limitCarsButton.Click += new System.EventHandler(this.limitCarsButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(665, 273);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Обмежити рух автомобілів";
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(823, 271);
            this.numericUpDown.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(58, 20);
            this.numericUpDown.TabIndex = 8;
            this.numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // normalWeatherRadioButton
            // 
            this.normalWeatherRadioButton.AutoSize = true;
            this.normalWeatherRadioButton.Checked = true;
            this.normalWeatherRadioButton.Location = new System.Drawing.Point(666, 230);
            this.normalWeatherRadioButton.Name = "normalWeatherRadioButton";
            this.normalWeatherRadioButton.Size = new System.Drawing.Size(121, 17);
            this.normalWeatherRadioButton.TabIndex = 9;
            this.normalWeatherRadioButton.TabStop = true;
            this.normalWeatherRadioButton.Text = "Нормальна погода";
            this.normalWeatherRadioButton.UseVisualStyleBackColor = true;
            this.normalWeatherRadioButton.CheckedChanged += new System.EventHandler(this.normalWeatherRadioButton_CheckedChanged);
            // 
            // rainyWeatherRadioButton
            // 
            this.rainyWeatherRadioButton.AutoSize = true;
            this.rainyWeatherRadioButton.Location = new System.Drawing.Point(793, 230);
            this.rainyWeatherRadioButton.Name = "rainyWeatherRadioButton";
            this.rainyWeatherRadioButton.Size = new System.Drawing.Size(49, 17);
            this.rainyWeatherRadioButton.TabIndex = 10;
            this.rainyWeatherRadioButton.TabStop = true;
            this.rainyWeatherRadioButton.Text = "Дощ";
            this.rainyWeatherRadioButton.UseVisualStyleBackColor = true;
            this.rainyWeatherRadioButton.CheckedChanged += new System.EventHandler(this.rainyWeatherRadioButton_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 372);
            this.Controls.Add(this.rainyWeatherRadioButton);
            this.Controls.Add(this.normalWeatherRadioButton);
            this.Controls.Add(this.numericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.limitCarsButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.nextDayButton);
            this.Controls.Add(this.addEnterpriseButton);
            this.Controls.Add(this.gmap);
            this.Name = "Form1";
            this.Text = "Контроль міського екологічного стану";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.Button addEnterpriseButton;
        private System.Windows.Forms.Label cityNameLabel;
        private System.Windows.Forms.Label elapsedDaysLabel;
        private System.Windows.Forms.Label currentEmissionsLabel;
        private System.Windows.Forms.Label permissibleEmissionsLabel;
        private System.Windows.Forms.Button nextDayButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label numberOfEnterprisesLabel;
        private System.Windows.Forms.Label budgetLabel;
        private System.Windows.Forms.Label penaltyDaysCarsLabel;
        private System.Windows.Forms.Label numberOfCarsLabel;
        private System.Windows.Forms.Button limitCarsButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.RadioButton normalWeatherRadioButton;
        private System.Windows.Forms.RadioButton rainyWeatherRadioButton;

    }
}

