﻿namespace EnvironmentalMonitoringModel
{
    partial class ShowEnterpriseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.penaltydaysLabel = new System.Windows.Forms.Label();
            this.isWorkingLabel = new System.Windows.Forms.Label();
            this.filtersLabel = new System.Windows.Forms.Label();
            this.budgetLabel = new System.Windows.Forms.Label();
            this.currentEmissionsLabel = new System.Windows.Forms.Label();
            this.taxLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.installFiltersButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.suspedWorkButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.penaltyButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // gmap
            // 
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = false;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(12, 12);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 13;
            this.gmap.MinZoom = 9;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(499, 349);
            this.gmap.TabIndex = 2;
            this.gmap.Zoom = 12D;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.penaltydaysLabel);
            this.groupBox1.Controls.Add(this.isWorkingLabel);
            this.groupBox1.Controls.Add(this.filtersLabel);
            this.groupBox1.Controls.Add(this.budgetLabel);
            this.groupBox1.Controls.Add(this.currentEmissionsLabel);
            this.groupBox1.Controls.Add(this.taxLabel);
            this.groupBox1.Controls.Add(this.nameLabel);
            this.groupBox1.Location = new System.Drawing.Point(517, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 183);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Інформація про підприємство";
            // 
            // penaltydaysLabel
            // 
            this.penaltydaysLabel.AutoSize = true;
            this.penaltydaysLabel.Location = new System.Drawing.Point(6, 160);
            this.penaltydaysLabel.Name = "penaltydaysLabel";
            this.penaltydaysLabel.Size = new System.Drawing.Size(135, 13);
            this.penaltydaysLabel.TabIndex = 0;
            this.penaltydaysLabel.Text = "Кількість штрафних днів: ";
            // 
            // isWorkingLabel
            // 
            this.isWorkingLabel.AutoSize = true;
            this.isWorkingLabel.Location = new System.Drawing.Point(6, 138);
            this.isWorkingLabel.Name = "isWorkingLabel";
            this.isWorkingLabel.Size = new System.Drawing.Size(47, 13);
            this.isWorkingLabel.TabIndex = 0;
            this.isWorkingLabel.Text = "Статус: ";
            // 
            // filtersLabel
            // 
            this.filtersLabel.AutoSize = true;
            this.filtersLabel.Location = new System.Drawing.Point(6, 115);
            this.filtersLabel.Name = "filtersLabel";
            this.filtersLabel.Size = new System.Drawing.Size(103, 13);
            this.filtersLabel.TabIndex = 0;
            this.filtersLabel.Text = "Кількість фільтрів: ";
            // 
            // budgetLabel
            // 
            this.budgetLabel.AutoSize = true;
            this.budgetLabel.Location = new System.Drawing.Point(6, 93);
            this.budgetLabel.Name = "budgetLabel";
            this.budgetLabel.Size = new System.Drawing.Size(53, 13);
            this.budgetLabel.TabIndex = 0;
            this.budgetLabel.Text = "Бюджет: ";
            // 
            // currentEmissionsLabel
            // 
            this.currentEmissionsLabel.AutoSize = true;
            this.currentEmissionsLabel.Location = new System.Drawing.Point(6, 70);
            this.currentEmissionsLabel.Name = "currentEmissionsLabel";
            this.currentEmissionsLabel.Size = new System.Drawing.Size(100, 13);
            this.currentEmissionsLabel.TabIndex = 0;
            this.currentEmissionsLabel.Text = "Кількість викидів: ";
            // 
            // taxLabel
            // 
            this.taxLabel.AutoSize = true;
            this.taxLabel.Location = new System.Drawing.Point(6, 48);
            this.taxLabel.Name = "taxLabel";
            this.taxLabel.Size = new System.Drawing.Size(109, 13);
            this.taxLabel.TabIndex = 0;
            this.taxLabel.Text = "Щоденний податок: ";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(6, 25);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(117, 13);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Назва підприємства: ";
            // 
            // installFiltersButton
            // 
            this.installFiltersButton.Location = new System.Drawing.Point(691, 201);
            this.installFiltersButton.Name = "installFiltersButton";
            this.installFiltersButton.Size = new System.Drawing.Size(62, 23);
            this.installFiltersButton.TabIndex = 4;
            this.installFiltersButton.Text = "ОК";
            this.installFiltersButton.UseVisualStyleBackColor = true;
            this.installFiltersButton.Click += new System.EventHandler(this.installFiltersButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(517, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Встановити фільтри";
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(631, 204);
            this.numericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown.TabIndex = 6;
            this.numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // suspedWorkButton
            // 
            this.suspedWorkButton.Location = new System.Drawing.Point(692, 240);
            this.suspedWorkButton.Name = "suspedWorkButton";
            this.suspedWorkButton.Size = new System.Drawing.Size(62, 23);
            this.suspedWorkButton.TabIndex = 4;
            this.suspedWorkButton.Text = "ОК";
            this.suspedWorkButton.UseVisualStyleBackColor = true;
            this.suspedWorkButton.Click += new System.EventHandler(this.suspendWorkButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(518, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Призупинити роботу";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(632, 243);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(49, 20);
            this.numericUpDown2.TabIndex = 6;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // penaltyButton
            // 
            this.penaltyButton.Location = new System.Drawing.Point(591, 278);
            this.penaltyButton.Name = "penaltyButton";
            this.penaltyButton.Size = new System.Drawing.Size(118, 23);
            this.penaltyButton.TabIndex = 7;
            this.penaltyButton.Text = "Зняти штраф";
            this.penaltyButton.UseVisualStyleBackColor = true;
            this.penaltyButton.Click += new System.EventHandler(this.penaltyButton_Click);
            // 
            // ShowEnterpriseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 373);
            this.Controls.Add(this.penaltyButton);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.suspedWorkButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.installFiltersButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gmap);
            this.Name = "ShowEnterpriseForm";
            this.Text = "Інформація про підприємство";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label taxLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label isWorkingLabel;
        private System.Windows.Forms.Label filtersLabel;
        private System.Windows.Forms.Label budgetLabel;
        private System.Windows.Forms.Label currentEmissionsLabel;
        private System.Windows.Forms.Label penaltydaysLabel;
        private System.Windows.Forms.Button installFiltersButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.Button suspedWorkButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Button penaltyButton;
    }
}