﻿using System;
using System.Collections.Generic;

namespace EnvironmentalMonitoringModel
{
    public abstract class Enterprise
    {
        public string Name;
        public int Tax;
        public int Emissions;
        public int Budget;
        public Location CurrentLocation;

        public int NumberOfFilters = 0;
        public int NumberOfPenaltyDays = 0;
        public bool IsWorking = true;

        protected List<Filter> filters = new List<Filter>();

        public Enterprise(string name, int tax, int emissions, int budget, Location currentLocation)
        {
            Name = name;
            Tax = tax;
            Emissions = emissions;
            Budget = budget;
            CurrentLocation = currentLocation;   
        }

        public abstract void InstallFilters(int number);
        public void ImposeSanctions(int numberOfPenaltyDays)
        {
            IsWorking = false;
            NumberOfPenaltyDays = numberOfPenaltyDays;
        }
        public void ImposeSanctions()
        {
            Budget -= 40000;
        }
        public void UpdateCondition()
        {
            if (!IsWorking && --NumberOfPenaltyDays == 0)
            {
                IsWorking = true;
            }

            foreach (var filter in filters)
            {
                filter.UpdateCondition();
                if (filter.IsActive && filter.IsNew)
                {
                    Emissions -= filter.TreatmentLevel;
                    filter.IsNew = false;
                    NumberOfFilters++;
                }
            }
            if (Budget < 0)
            {
                IsWorking = false;
                NumberOfPenaltyDays = 0;
            }
            CheckFields();
        }
        public int CurrentEmissions
        {
            get
            {
                Random rand = new Random();
                return Emissions + Emissions * rand.Next(-5, 5) / 100;
            }
        }
        public void CheckFields()
        {
            Emissions = Emissions > 0 ? Emissions : 0;
            Budget = Budget > 0 ? Budget : 0;
        }
    }
}
