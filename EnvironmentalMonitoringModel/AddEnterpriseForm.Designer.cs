﻿namespace EnvironmentalMonitoringModel
{
    partial class AddEnterpriseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.taxTextBox = new System.Windows.Forms.TextBox();
            this.emissionsTextBox = new System.Windows.Forms.TextBox();
            this.budgetTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.addEnterpriseButton = new System.Windows.Forms.Button();
            this.bigRadioButton = new System.Windows.Forms.RadioButton();
            this.smallRadioButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // gmap
            // 
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = false;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(12, 12);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 13;
            this.gmap.MinZoom = 9;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(499, 349);
            this.gmap.TabIndex = 1;
            this.gmap.Zoom = 12D;
            this.gmap.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gmap_MouseClick);
            // 
            // taxTextBox
            // 
            this.taxTextBox.Location = new System.Drawing.Point(711, 33);
            this.taxTextBox.Name = "taxTextBox";
            this.taxTextBox.Size = new System.Drawing.Size(87, 20);
            this.taxTextBox.TabIndex = 2;
            // 
            // emissionsTextBox
            // 
            this.emissionsTextBox.Location = new System.Drawing.Point(711, 59);
            this.emissionsTextBox.Name = "emissionsTextBox";
            this.emissionsTextBox.Size = new System.Drawing.Size(87, 20);
            this.emissionsTextBox.TabIndex = 2;
            // 
            // budgetTextBox
            // 
            this.budgetTextBox.Location = new System.Drawing.Point(711, 85);
            this.budgetTextBox.Name = "budgetTextBox";
            this.budgetTextBox.Size = new System.Drawing.Size(87, 20);
            this.budgetTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(531, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Щоденний податок";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(531, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Кількість щоденних викидів";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(531, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Бюджет";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(711, 9);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(87, 20);
            this.nameTextBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(531, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Назва";
            // 
            // addEnterpriseButton
            // 
            this.addEnterpriseButton.Location = new System.Drawing.Point(591, 157);
            this.addEnterpriseButton.Name = "addEnterpriseButton";
            this.addEnterpriseButton.Size = new System.Drawing.Size(132, 23);
            this.addEnterpriseButton.TabIndex = 4;
            this.addEnterpriseButton.Text = "Додати підприємство";
            this.addEnterpriseButton.UseVisualStyleBackColor = true;
            this.addEnterpriseButton.Click += new System.EventHandler(this.addEnterpriseButton_Click);
            // 
            // bigRadioButton
            // 
            this.bigRadioButton.AutoSize = true;
            this.bigRadioButton.Checked = true;
            this.bigRadioButton.Location = new System.Drawing.Point(534, 120);
            this.bigRadioButton.Name = "bigRadioButton";
            this.bigRadioButton.Size = new System.Drawing.Size(134, 17);
            this.bigRadioButton.TabIndex = 5;
            this.bigRadioButton.TabStop = true;
            this.bigRadioButton.Text = "Велике підприємство";
            this.bigRadioButton.UseVisualStyleBackColor = true;
            // 
            // smallRadioButton
            // 
            this.smallRadioButton.AutoSize = true;
            this.smallRadioButton.Location = new System.Drawing.Point(674, 120);
            this.smallRadioButton.Name = "smallRadioButton";
            this.smallRadioButton.Size = new System.Drawing.Size(124, 17);
            this.smallRadioButton.TabIndex = 5;
            this.smallRadioButton.Text = "Мале підприємство";
            this.smallRadioButton.UseVisualStyleBackColor = true;
            // 
            // AddEnterpriseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 388);
            this.Controls.Add(this.smallRadioButton);
            this.Controls.Add(this.bigRadioButton);
            this.Controls.Add(this.addEnterpriseButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.budgetTextBox);
            this.Controls.Add(this.emissionsTextBox);
            this.Controls.Add(this.taxTextBox);
            this.Controls.Add(this.gmap);
            this.Name = "AddEnterpriseForm";
            this.Text = "Додати підприємство";
            this.Load += new System.EventHandler(this.AddEnterpriseForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.TextBox taxTextBox;
        private System.Windows.Forms.TextBox emissionsTextBox;
        private System.Windows.Forms.TextBox budgetTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button addEnterpriseButton;
        private System.Windows.Forms.RadioButton bigRadioButton;
        private System.Windows.Forms.RadioButton smallRadioButton;

    }
}