﻿
namespace EnvironmentalMonitoringModel
{
    interface IEnterprise
    {
        void UpdateCondition();
        void InstallFilters(int number);
        void ImposeSanctions(int numberOfPenaltyDays);
    }
}
