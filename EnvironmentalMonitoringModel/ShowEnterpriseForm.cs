﻿using System;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace EnvironmentalMonitoringModel
{
    public partial class ShowEnterpriseForm : Form
    {
        private Enterprise enterprise;

        public ShowEnterpriseForm(Enterprise enterprise)
        {
            InitializeComponent();
            this.enterprise = enterprise;

            InitLabels();
            InitMap();
        }
        public void InitLabels()
        {
            nameLabel.Text += enterprise.Name;
            taxLabel.Text += enterprise.Tax;
            currentEmissionsLabel.Text += enterprise.CurrentEmissions;
            budgetLabel.Text += enterprise.Budget;
            filtersLabel.Text += enterprise.NumberOfFilters;
            penaltydaysLabel.Text += enterprise.NumberOfPenaltyDays;
            if (enterprise.IsWorking)
            {
                isWorkingLabel.Text += "Працює";
            }
            else
            {
                isWorkingLabel.Text += "Не працює";
            }
        }
        public void InitMap()
        {
            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.Position = new PointLatLng(49.428944, 32.056601);
            GMapOverlay markersOverlay = new GMapOverlay("markers");
            gmap.Overlays.Add(markersOverlay);
            Location location = enterprise.CurrentLocation;
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(location.Latitude, location.Longitude),
                GMarkerGoogleType.blue);
            markersOverlay.Markers.Add(marker);
        }
        private void installFiltersButton_Click(object sender, EventArgs e)
        {
            int numberOfFilters = Convert.ToInt32(numericUpDown.Value);
            if (enterprise.Budget > 30000)
            {
                enterprise.InstallFilters(numberOfFilters);
                MessageBox.Show("Фільтер буде встановлено через 7 днів");
            }
            else
            {
                MessageBox.Show("Неможливо встановити фільтр. Вартість одного фільтра 30000 у.о.");
            }
        }
        private void suspendWorkButton_Click(object sender, EventArgs e)
        {
            int numberOfDays = Convert.ToInt32(numericUpDown2.Value);
            enterprise.ImposeSanctions(numberOfDays);
            MessageBox.Show("Роботу підприємства призупинено");
            isWorkingLabel.Text = "Статус: Роботу призупинено";
            penaltydaysLabel.Text = "Кількість штрафних днів: " + numberOfDays;
        }
        private void penaltyButton_Click(object sender, EventArgs e)
        {
            (Owner as Form1).City.Budget += 40000;
            enterprise.ImposeSanctions();
        }
    }
}
