﻿using System;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace EnvironmentalMonitoringModel
{
    public partial class Form1 : Form
    {
        public City City;
        private int ElapsedDays;
        private GMapOverlay markersOverlay;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            InitMap();
            CityInfoForm cityInfoForm = new CityInfoForm();
            cityInfoForm.Owner = this;
            cityInfoForm.ShowDialog();
            InitLabels();
        }
        public void AddMarker(Enterprise enterprise)
        {
            Location location = enterprise.CurrentLocation;
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(location.Latitude, location.Longitude),
                GMarkerGoogleType.blue);
            marker.ToolTipMode = MarkerTooltipMode.Never;
            marker.ToolTipText = enterprise.Name;
            markersOverlay.Markers.Add(marker);
        }
        public void SetCity(City c)
        {
            City = c;
        }
        public void AddEnterprise(Enterprise enterprise)
        {
            City.AddEnterprise(enterprise);
            AddMarker(enterprise);
            numberOfEnterprisesLabel.Text = "Кількість підприємств " + City.NumberOfEnterprises;
        }
        public void InitLabels()
        {
            cityNameLabel.Text = "Місто: " + City.Name;
            permissibleEmissionsLabel.Text = "Кількість допустимих викидів: " + City.PermissibleEmissions;
            currentEmissionsLabel.Text = "Кількість викидів на данний момент: " + City.CurrentEmissions;
            budgetLabel.Text = "Бюджет міста: " + City.Budget;
            numberOfCarsLabel.Text = "Кількість автомобілів: " + City.NumberOfCars;
        }
        public void InitMap()
        {
            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.Position = new PointLatLng(49.428944, 32.056601);
            markersOverlay = new GMapOverlay("markers");
            gmap.Overlays.Add(markersOverlay);
        }
        private void nextDayButton_Click(object sender, EventArgs e)
        {
            City.UpdateCondition();
            markersOverlay.Clear();
            foreach (Enterprise en in City.Enterprises)
            {
                AddMarker(en);
            }
            ++ElapsedDays;
            elapsedDaysLabel.Text = "Пройшло днів: " + ElapsedDays;
            currentEmissionsLabel.Text = "Кількість викидів на данний момент: " + City.CurrentEmissions;
            budgetLabel.Text = "Бюджет міста: " + City.Budget;
            numberOfCarsLabel.Text = "Кількість автомобілів: " + City.NumberOfCars;
            if (City.CurrentEmissions > City.PermissibleEmissions)
            {
                MessageBox.Show("Перевищена кількість допустимих викидів.\n" + 
                    "Встановіть фільтри на підприємства, або введіть обмеження на рух автомобілів.");
            } 
        }
        private void gmap_OnMarkerClick(GMapMarker item, MouseEventArgs e)
        {
            Enterprise en = City.Enterprises.Find(x => x.Name.Contains(item.ToolTipText));
            ShowEnterpriseForm enterpriseForm = new ShowEnterpriseForm(en);
            enterpriseForm.Owner = this;
            enterpriseForm.ShowDialog();
        }
        private void limitCarsButton_Click(object sender, EventArgs e)
        {
            int numberOfDays = Convert.ToInt32(numericUpDown.Value);
            City.LimitNumberOfCars(numberOfDays);
            numberOfCarsLabel.Text = "Кількість автомобілів: " + City.NumberOfCars;
            penaltyDaysCarsLabel.Text = "Обмеження руху автомобілів: " + City.NumberOfPenaltyDaysForCars + " днів";
            numericUpDown.Value = 1;
        }
        private void addEnterpriseButton_Click(object sender, EventArgs e)
        {
            AddEnterpriseForm addEnterpriseForm = new AddEnterpriseForm();
            addEnterpriseForm.Owner = this;
            addEnterpriseForm.ShowDialog();
        }
        private void Form1_Activated(object sender, EventArgs e)
        {
            InitLabels();
        }
        private void normalWeatherRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            City.WeatherFactor = 0.2;
        }
        private void rainyWeatherRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            City.WeatherFactor = 0.5;
        }
    }
}
