﻿using System;
using System.Windows.Forms;

namespace EnvironmentalMonitoringModel
{
    public partial class CityInfoForm : Form
    {
        public CityInfoForm()
        {
            InitializeComponent();
        }
        private void okButton_Click(object sender, EventArgs e)
        {
            int permissibleEmissions = Convert.ToInt32(permissibleEmissionsTextBox.Text);
            int budget = Convert.ToInt32(budgetTextBox.Text);
            int numberOfCars = Convert.ToInt32(numberOfCarsTextBox.Text);
            City c = new City("Черкаси", permissibleEmissions, numberOfCars, budget);
            (Owner as Form1).SetCity(c);
            this.Close();
        }
    }
}
