﻿using System.Collections.Generic;

namespace EnvironmentalMonitoringModel
{
    public class City
    {
        public List<Enterprise> Enterprises = new List<Enterprise>();

        public string Name;
        public int PermissibleEmissions;
        public int CurrentEmissions = 0;
        public int Budget = 0;

        // WeatherFactor affects the elimination of emissions.
        // It could be 0.2 under normal conditions and
        // 0.5 in case of rain/wind.
        public double WeatherFactor = 0.2;

        public int NumberOfCars;
        public int NumberOfPenaltyDaysForCars = 0;
        public int NumberOfEnterprises = 0;

        public City(string name, int permissibleEmissions, int numberOfCars, int budget)
        {
            Name = name;
            PermissibleEmissions = permissibleEmissions;
            NumberOfCars = numberOfCars;
            Budget = budget;
            CurrentEmissions += numberOfCars * 10;
        }
        public void AddEnterprise(Enterprise enterprise)
        {
            Enterprises.Add(enterprise);
            NumberOfEnterprises++;
        }
        public void LimitNumberOfCars(int numberOfDays)
        {
            NumberOfCars /= 2;
            NumberOfPenaltyDaysForCars = numberOfDays;
        }
        public void ImposeSanctions(Enterprise enterprise)
        {
            Budget += 40000;
            enterprise.Budget -= 40000;
        }
        public void ImposeSanctions(Enterprise enterprise, int numberOfDays)
        {
            enterprise.ImposeSanctions(numberOfDays);
        }
        public void UpdateCondition()
        {
            if (--NumberOfPenaltyDaysForCars == 0)
            {
                NumberOfCars *= 2;
            }
            
            CurrentEmissions += (int)(NumberOfCars * 0.75 * 10);

            foreach (Enterprise enterprise in Enterprises)
            {
                enterprise.UpdateCondition();
                if (enterprise.IsWorking)
                {
                    CurrentEmissions += enterprise.Emissions;
                    Budget += enterprise.Tax;
                }
            }

            CurrentEmissions -= (int)(CurrentEmissions * WeatherFactor);

            CheckFields();
        }
        public void CheckFields()
        {
            NumberOfCars = NumberOfCars > 0 ? NumberOfCars : 0;
            CurrentEmissions = CurrentEmissions > 0 ? CurrentEmissions : 0;
            Budget = Budget > 0 ? Budget : 0;
        }
    }
}
