﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnvironmentalMonitoringModel
{
    class BigEnterprise : Enterprise
    {
        public BigEnterprise(string name, int tax, int emissions, int budget, Location currentLocation)
            : base(name, tax, emissions, budget, currentLocation) { }

        public override void InstallFilters(int number)
        {
            Budget -= 20000;
            filters.Add(new BigFilter());
        }
    }
}
