﻿using System;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace EnvironmentalMonitoringModel
{
    public partial class AddEnterpriseForm : Form
    {
        private GMapOverlay markersOverlay;
        private Location location;

        public AddEnterpriseForm()
        {
            InitializeComponent();
        }
        private void AddEnterpriseForm_Load(object sender, EventArgs e)
        {
            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.Position = new PointLatLng(49.428944, 32.056601);
            markersOverlay = new GMapOverlay("markers");
            gmap.Overlays.Add(markersOverlay);
        }
        private void gmap_MouseClick(object sender, MouseEventArgs e)
        {
            base.OnClick(e);

            markersOverlay.Clear();
            double latitude = gmap.FromLocalToLatLng(e.X, e.Y).Lat;
            double longitude = gmap.FromLocalToLatLng(e.X, e.Y).Lng;

            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(latitude, longitude),
                GMarkerGoogleType.blue);
            markersOverlay.Markers.Add(marker);

            location = new Location(latitude, longitude);
        }
        private void addEnterpriseButton_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text;
            int tax = Int32.Parse(taxTextBox.Text);
            int emissions = Int32.Parse(emissionsTextBox.Text);
            int budget = Int32.Parse(budgetTextBox.Text);

            Enterprise newEnterprise;

            if (bigRadioButton.Checked)
            {
                newEnterprise = new BigEnterprise(name, tax, emissions, budget, location);
            }
            else
            {
                newEnterprise = new SmallEnterprise(name, tax, emissions, budget, location);
            }

            (Owner as Form1).AddEnterprise(newEnterprise);
            this.Close();
        }
    }
}
