﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnvironmentalMonitoringModel
{
    class SmallEnterprise : Enterprise
    {
        public SmallEnterprise(string name, int tax, int emissions, int budget, Location currentLocation)
            : base(name, tax, emissions, budget, currentLocation) { }

        public override void InstallFilters(int number)
        {
            Budget -= 5000;
            filters.Add(new SmallFilter());
        }
    }
}
