﻿
namespace EnvironmentalMonitoringModel
{
    public abstract class Filter
    {
        public bool IsActive = false;
        public bool IsNew = true;
        private int DaysToInstall = 7;

        public abstract int TreatmentLevel { get; }

        public void UpdateCondition()
        {
            if (!IsActive && --DaysToInstall == 0)
            {
                IsActive = true;
            }
        }

    }
}
