﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnvironmentalMonitoringModel
{
    class SmallFilter : Filter
    {
        public override int TreatmentLevel
        {
            get { return 5000; }
        }
    }
}
