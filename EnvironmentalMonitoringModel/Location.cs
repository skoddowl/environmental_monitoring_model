﻿
namespace EnvironmentalMonitoringModel
{
    public class Location
    {
        public double Latitude;
        public double Longitude;

        public Location(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}
