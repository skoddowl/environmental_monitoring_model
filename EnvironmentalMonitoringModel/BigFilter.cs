﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnvironmentalMonitoringModel
{
    class BigFilter : Filter
    {
        public override int TreatmentLevel
        {
            get { return 20000; }
        }
    }
}
